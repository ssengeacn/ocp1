FROM ubuntu:18.04

ENV CONFLUENT_VERSION=4.0 \
    JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64

RUN apt-get update && \
    apt-get install -y \
      sudo \
      iputils-ping && \
    chmod 4755 /bin/ping && \
    rm -rf /var/lib/apt/lists/*

RUN set -x \
    && apt-get update \
    && apt-get install -y openjdk-8-jre-headless wget netcat-openbsd software-properties-common \
    && wget -qO - http://packages.confluent.io/deb/$CONFLUENT_VERSION/archive.key | apt-key add - \
    && add-apt-repository "deb [arch=amd64] http://packages.confluent.io/deb/$CONFLUENT_VERSION stable main" \
    && apt-get update \
    && apt-get install -y confluent-platform-oss-2.11

CMD exec /bin/bash -c "trap : TERM INT; sleep infinity & wait"
